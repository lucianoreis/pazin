<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'db_entity' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
                'paths' => array(
                    0 => __DIR__ . '/../src/Loja/Entity',
                ),
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Loja\\Entity' => 'db_entity',
                ),
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'loja.rest.doctrine.produto' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/produtos[/:produto_id]',
                    'defaults' => array(
                        'controller' => 'Loja\\V1\\Rest\\Produto\\Controller',
                    ),
                ),
            ),
            'loja.rest.doctrine.categoria' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/categorias[/:categoria_id]',
                    'defaults' => array(
                        'controller' => 'Loja\\V1\\Rest\\Categoria\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'loja.rest.doctrine.produto',
            1 => 'loja.rest.doctrine.categoria',
        ),
    ),
    'zf-rest' => array(
        'Loja\\V1\\Rest\\Produto\\Controller' => array(
            'listener' => 'Loja\\V1\\Rest\\Produto\\ProdutoResource',
            'route_name' => 'loja.rest.doctrine.produto',
            'route_identifier_name' => 'produto_id',
            'entity_identifier_name' => 'id',
            'collection_name' => 'produto',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Loja\\Entity\\Produto',
            'collection_class' => 'Loja\\V1\\Rest\\Produto\\ProdutoCollection',
            'service_name' => 'Produto',
        ),
        'Loja\\V1\\Rest\\Categoria\\Controller' => array(
            'listener' => 'Loja\\V1\\Rest\\Categoria\\CategoriaResource',
            'route_name' => 'loja.rest.doctrine.categoria',
            'route_identifier_name' => 'categoria_id',
            'entity_identifier_name' => 'id',
            'collection_name' => 'categoria',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Loja\\Entity\\Categoria',
            'collection_class' => 'Loja\\V1\\Rest\\Categoria\\CategoriaCollection',
            'service_name' => 'Categoria',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Loja\\V1\\Rest\\Produto\\Controller' => 'HalJson',
            'Loja\\V1\\Rest\\Categoria\\Controller' => 'HalJson',
        ),
        'accept-whitelist' => array(
            'Loja\\V1\\Rest\\Produto\\Controller' => array(
                0 => 'application/vnd.loja.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Loja\\V1\\Rest\\Categoria\\Controller' => array(
                0 => 'application/vnd.loja.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content-type-whitelist' => array(
            'Loja\\V1\\Rest\\Produto\\Controller' => array(
                0 => 'application/vnd.loja.v1+json',
                1 => 'application/json',
            ),
            'Loja\\V1\\Rest\\Categoria\\Controller' => array(
                0 => 'application/vnd.loja.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Loja\\Entity\\Produto' => array(
                'route_identifier_name' => 'produto_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'loja.rest.doctrine.produto',
                'hydrator' => 'Loja\\Hydrator\\ProdutoHydrator',
            ),
            'Loja\\V1\\Rest\\Produto\\ProdutoCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'loja.rest.doctrine.produto',
                'is_collection' => true,
            ),
            'Loja\\Entity\\Categoria' => array(
                'route_identifier_name' => 'categoria_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'loja.rest.doctrine.categoria',
                'hydrator' => 'Loja\\Hydrator\\CategoriaHydrator',
            ),
            'Loja\\V1\\Rest\\Categoria\\CategoriaCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'loja.rest.doctrine.categoria',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-apigility' => array(
        'doctrine-connected' => array(
            'Loja\\V1\\Rest\\Produto\\ProdutoResource' => array(
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'Loja\\V1\\Rest\\Produto\\ProdutoHydrator',
            ),
            'Loja\\V1\\Rest\\Categoria\\CategoriaResource' => array(
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'Loja\\V1\\Rest\\Categoria\\CategoriaHydrator',
            ),
        ),
    ),
    'doctrine-hydrator' => array(
        'Loja\\V1\\Rest\\Produto\\ProdutoHydrator' => array(
            'entity_class' => 'Loja\\Entity\\Produto',
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => array(),
            'use_generated_hydrator' => true,
        ),
        'Loja\\V1\\Rest\\Categoria\\CategoriaHydrator' => array(
            'entity_class' => 'Loja\\Entity\\Categoria',
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => array(),
            'use_generated_hydrator' => true,
        ),
    ),
    'zf-content-validation' => array(
        'Loja\\V1\\Rest\\Produto\\Controller' => array(
            'input_filter' => 'Loja\\V1\\Rest\\Produto\\Validator',
        ),
        'Loja\\V1\\Rest\\Categoria\\Controller' => array(
            'input_filter' => 'Loja\\V1\\Rest\\Categoria\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Loja\\V1\\Rest\\Produto\\Validator' => array(
            0 => array(
                'name' => 'nome',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'descricao',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'estoque',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
        ),
        'Loja\\V1\\Rest\\Categoria\\Validator' => array(
            0 => array(
                'name' => 'nome',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
        ),
    ),
);
