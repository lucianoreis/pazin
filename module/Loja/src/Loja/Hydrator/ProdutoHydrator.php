<?php
namespace Loja\Hydrator;

use Loja\Entity\Produto;
use Loja\Repository\ProdutoRepository;
use \Zend\Stdlib\Hydrator\AbstractHydrator;
use Zend\Stdlib\Exception;

class ProdutoHydrator extends AbstractHydrator{

    private $entityRepository;

    public function extract($object)
    {
        $entity = $object;

        $data = array();
        $data['id']              = $entity->getId();
        $data['nome']            = $entity->getNome();
        $data['descricao']       = $entity->getDescricao();
        $data['categoria']       = $entity->getCategoria()->getNome();
        $data['estoque']         = $entity->getEstoque();

        return $data;
    }

    public function hydrate(array $data, $object)
    {
        if (!($object instanceof Produto)) {
            throw new Exception\BadMethodCallException(sprintf(
                '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }

        if(array_key_exists('nome',$data))            $object->setNome($data['nome']);
        if(array_key_exists('descricao',$data))       $object->setDescricao($data['descricao']);
        if(array_key_exists('categoria',$data))       $object->setCategoria($data['categoria']);
        if(array_key_exists('estoque',$data))         $object->setEstoque($data['estoque']);

        return $object;
    }

    public function setUserRepo(UserRepository $repository)
    {
        $this->entityRepository = $repository;
    }
}