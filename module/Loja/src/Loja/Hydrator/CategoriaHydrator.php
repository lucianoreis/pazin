<?php
namespace Loja\Hydrator;

use Loja\Entity\Categoria;
use Loja\Repository\PaisRepository;
use \Zend\Stdlib\Hydrator\AbstractHydrator;
use Zend\Stdlib\Exception;

class CategoriaHydrator extends AbstractHydrator{

    private $entityRepository;

    public function extract($object)
    {
        $entity = $object;

        $data = array();
        $data['id']       = $entity->getId();
        $data['nome']     = $entity->getNome();

        return $data;
    }

    public function hydrate(array $data, $object)
    {
        if (!($object instanceof Categoria)) {
            throw new Exception\BadMethodCallException(sprintf(
                '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }

        if(array_key_exists('nome',$data))             $object->setNome($data['nome']);

        return $object;
    }

    public function setPaisRepo(PaisRepository $repository)
    {
        $this->entityRepository = $repository;
    }
}