<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Uri\UriFactory as UriFactory;
use Doctrine\ORM\EntityManager as DoctrineEntityManager;

class Module
{
    static private $entityManager;
    static public function getEntityManager()
    {
        return self::$entityManager;
    }
 
    static public function setEntityManager(DoctrineEntityManager $entityManager)
    {
        self::$entityManager = $entityManager;
    }

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        UriFactory::registerScheme('chrome-extension', 'Zend\Uri\Uri');
        self::setEntityManager($e->getApplication()->getServiceManager()->get('doctrine.entitymanager.orm_default'));
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
